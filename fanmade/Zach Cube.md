# Wildfire Warlord (Zach) Suggested banlist and amendments

## Banned

1. [Vortex](https://exodus-players.com/cards/search/Vortex-Unlimited)
2. Reevaluate
3. Repel the Thoughtstream
4. Existence of Unknown Origins
5. Anathema

## Limited (1 copy)

1. Glimpse of the Sanctuary
2. Chains
3. Crystal Cove
4. Hailfire and Brimstone
5. Crossfire Formula
6. Maelstrom
7. Eroda's Ascendance
8. Manuscript of the Archivist
9. Hazardous Brigantine

## Amplified (3 copies)

1. Leech
2. Unsummon
3. Reversion
4. Rose's Reach
5. Star Struggle
